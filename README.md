# Diseño y Programación Web

Diseño y Programación Web


### Slides:

* [Presentación del curso](http://jonmircha.github.io/slides-web/#/)
* [Introducción a CSS](http://jonmircha.github.io/slides-css/#/)
* [Modelo de Caja CSS](http://jonmircha.github.io/slides-css/#/3)
* [Programación Orientada a Objetos en JavaScript](http://jonmircha.github.io/slides-poo-js/#/)
* [Introducción a los entornos Backend y los Servidores Web](http://jonmircha.github.io/slides-servidor-web/#/)
* [Patrón MVC](http://jonmircha.github.io/slides-mvc/)
* [Conexión a MySQL con PHP](http://jonmircha.github.io/slides-mvc/#/2)
